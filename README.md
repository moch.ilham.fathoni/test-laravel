## Cara menjalankan project di localhost (Windows)

- Install git (jika belum ada), xampp (jika belum ada), composer (jika belum ada) dan nodejs (jika belum ada).
- Buka Git Bash terminal kemudian buka folder untuk menaruh project ini, kemudian jalankan perintah dibawah ini.
- git clone https://gitlab.com/moch.ilham.fathoni/test-laravel.git
- cd test-laravel
- composer install
- npm install
- cp .env.example .env
- php artisan key:generate
- kemudian buka phpmyadmin dan buat database dengan nama masjidpedia
- check konfigurasi koneksi ke database pada file .env jika sudah benar maka jalankan perintah dibawah ini.
- php artisan migrate
- php artisan db:seed
- php artisan passport:install
- php artisan serve

## Konfigurasi kirim email dengan mailtrap.io

- klik [disini](https://mailtrap.io/inboxes).
- pilih salah satu inbox, kemudian pada tab SMTP Settings di Integrations pilih Laravel 7+
- copy konfigurasi yang muncul pada text area dan pastekan / replace di .env

## Dokumentasi REST API

Untuk REST API user toko bisa dilihat [disini](https://www.postman.com/rma-123/workspace/16873487-83b5-4f99-ab5d-17acd66f5bfd).
