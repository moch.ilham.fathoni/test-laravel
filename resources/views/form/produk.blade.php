@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8 col-sm-10">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4>
                    @if (!empty($new))
                        {{ $new }}
                    @elseif (!empty($edit))
                        {{ $edit }}
                    @elseif (!empty($view))
                        {{ $view }}
                    @endif Produk
                    </h4>
                </div>

                <form @if (!empty($new)) method="POST" action="{{ route('produk.store') }}" @elseif (!empty($edit)) method="POST" action="{{ route('produk.update', $data->id) }}" @endif>
                @if (!empty($edit))
                    @method('PATCH')
                @endif
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="nama" class="col-sm-4 col-form-label">Nama Produk</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nama" name="nama" @if (!empty($edit) or !empty($view)) value="{{ $data->nama }}" @endif  @if (!empty($view)) disabled @endif required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jumlah" class="col-sm-4 col-form-label">Jumlah Produk</label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" id="jumlah" name="jumlah" @if (!empty($edit) or !empty($view)) value="{{ $data->jumlah }}" @endif @if (!empty($view)) disabled @endif required>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        @if (!empty($new) or !empty($edit))
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        @endif
                        <a href="{{ route('home') }}" class="btn btn-secondary float-right">Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection