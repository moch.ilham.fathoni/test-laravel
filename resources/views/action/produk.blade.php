<a class="btn btn-primary" href="{{ route('produk.show', $model->id) }}">
    <i class="fas fa-search"></i>&nbsp;View
</a>
<a class="btn btn-success" href="{{ route('produk.edit', $model->id) }}">
    <i class="fas fa-edit"></i>&nbsp;Edit
</a>
<button type="submit" form="delete_produk_{{ $model->id }}" class="btn btn-danger"
    onclick="return confirm('Apakah ingin menghapus produk ini ?');">
    <i class="fas fa-trash"></i>&nbsp;Delete
</button>
<form id="delete_produk_{{ $model->id }}" action="{{ route('produk.destroy', $model->id)}}"
    method="post">
    @csrf
    @method('DELETE')
</form>
