<button type="submit" form="update_order_{{ $model->id }}" class="btn btn-primary"
    onclick="return confirm('Apakah ingin melakukan approval terhadap order ini ?');">
    <i class="fas fa-clipboard-check"></i>&nbsp;Approve
</button>
<form id="update_order_{{ $model->id }}" action="{{ route('order.update', $model->id) }}"
    method="post">
    @csrf
    @method('PATCH')
</form>