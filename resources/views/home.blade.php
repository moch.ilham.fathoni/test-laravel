@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4>{{ __('Data Produk Supplier') }}</h4></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success mb-4" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('produk.create') }}" class="d-inline-block btn btn-sm btn-primary m-3">
                        <i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data
                    </a>
        
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm table-hover" id="myTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Produk</th>
                                    <th>Stock Produk</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('produk.index') }}",
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'nama', name: 'nama' },
                { data: 'jumlah', name: 'jumlah' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ],
            columnDefs: [
                { targets: 0, className: 'text-center' },
                { targets: 3, className: 'text-center' }
            ]
        });
    });
</script>
@endpush