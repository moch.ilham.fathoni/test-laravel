<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\UserTokoController;
use App\Http\Controllers\Auth\AuthenticationUserTokoController;
use App\Http\Controllers\Api\SupplierController;
use App\Http\Controllers\Api\ProdukSupplierController;
use App\Http\Controllers\Api\ProdukTokoController;
use App\Http\Controllers\Api\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [UserTokoController::class, 'register']);
Route::post('/login', [UserTokoController::class, 'login']);
Route::post('/forgot-password', [AuthenticationUserTokoController::class, 'forgotPassword']);
Route::post('/reset-password', [AuthenticationUserTokoController::class, 'reset']);

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', [UserTokoController::class, 'logout']);
    Route::apiResource('/supplier', SupplierController::class);
    Route::get('/supplier-produk', [ProdukSupplierController::class, 'index']);
    Route::get('/toko-produk', [ProdukTokoController::class, 'index']);
    Route::post('/order', [OrderController::class, 'simpan']);
});