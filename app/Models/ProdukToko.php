<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProdukToko extends Model
{
    use HasFactory;

    protected $table = 'produk_toko';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'id_produk',
        'jumlah',
        'updated_at',
    ];

    /**
     * Get the toko that owns the produk.
     */
    public function toko()
    {
        return $this->belongsTo(UserToko::class, 'id_user');
    }

    /**
     * Get the produk supplier that owns the produk toko.
     */
    public function produksupplier()
    {
        return $this->belongsTo(Produk::class, 'id_produk');
    }
}
