<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'nama',
        'jumlah',
        'updated_at',
    ];

    /**
     * Get the supplier that owns the produk.
     */
    public function supplier()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
}
