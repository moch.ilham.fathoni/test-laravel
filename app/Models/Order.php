<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_supplier',
        'id_produk',
        'id_toko',
        'jumlah',
        'tanggal',
        'approve',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'approve' => 'boolean',
    ];

    /**
     * Get the supplier that owns the order.
     */
    public function supplier()
    {
        return $this->belongsTo(User::class, 'id_supplier');
    }

    /**
     * Get the produk that owns the order.
     */
    public function produk()
    {
        return $this->belongsTo(Produk::class, 'id_produk');
    }

    /**
     * Get the toko that owns the order.
     */
    public function toko()
    {
        return $this->belongsTo(UserToko::class, 'id_toko');
    }
}
