<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MailResetPasswordUserTokoNotification extends Notification
{
    use Queueable;

    /**
    * Create a new notification instance.
    *
    * @return void
    */
    protected $reset_code;
    public function __construct($code)
    {
        $this->reset_code = $code;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject('Reset Application Password')
        ->line('You are receiving this email because we received a password reset request for your account.')
        ->action('Click to get token!', 'localhost:8080?token=' . $this->reset_code)
        ->line('If you did not request a password reset, no further action is required.')
        ->subject('Admin - Password Reset Request');
    }
}