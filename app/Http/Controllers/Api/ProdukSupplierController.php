<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ApiResource;
use App\Models\Produk;

class ProdukSupplierController extends Controller
{
    public function index(Request $request)
    {
        try {
            $data = Produk::select('id as id_produk', 'id_user as id_supplier', 'nama', 'jumlah as stock')->where('id_user', '=', $request->get('id_supplier'))->get();
            return response(['success' => true, 'produk' => ApiResource::collection($data), 'message' => 'Data berhasil ditampilkan'], 200);
        } catch (\Exception $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        } catch (\Throwable $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        }
    }
}