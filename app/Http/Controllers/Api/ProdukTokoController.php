<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ApiResource;
use App\Models\ProdukToko;

class ProdukTokoController extends Controller
{
    public function index()
    {
        try {
            $data = ProdukToko::select('produk_toko.id_produk', 'produk_toko.id_user as id_supplier', 'produk.nama', 'produk_toko.jumlah as stock')
                              ->leftJoin('produk', function($join) {
                                    $join->on('produk_toko.id_produk', '=', 'produk.id');
                               })
                              ->get();
            return response(['success' => true, 'produk' => ApiResource::collection($data), 'message' => 'Data berhasil ditampilkan'], 200);
        } catch (\Exception $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        } catch (\Throwable $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        }
    }
}
