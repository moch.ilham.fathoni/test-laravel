<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends Controller
{
    public function simpan(Request $request)
    {
        try {
            $data = $request->json()->all();
            $order = $data['order'];
            foreach ($order as $value) {
                Order::create([
                    'id_supplier'   => $value['id_supplier'],
                    'id_produk'     => $value['id_produk'],
                    'id_toko'       => $request->user()->id,
                    'jumlah'        => $value['jumlah'],
                    'tanggal'       => $value['tanggal'],
                ]);
            }
            return response(['success' => true, 'message' => 'Pesanan berhasil diterima, mohon menunggu konfirmasi dari supplier terima kasih'], 200);
        } catch (\Exception $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        } catch (\Throwable $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        }
    }
}
