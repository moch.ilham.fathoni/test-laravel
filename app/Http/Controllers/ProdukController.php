<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class ProdukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:web', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Produk::where('id_user', '=', Auth::guard('web')->id())->get();
        return Datatables::of($data)
                         ->addIndexColumn()
                         ->addColumn('action', function ($model) {
                            return view('action.produk', compact('model'))->render();
                         })
                         ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $new = 'Input';
        return view('form.produk', compact('new'))->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'      => ['required', 'string', 'max:255'],
            'jumlah'    => ['required'],
        ]);
        try {
            Produk::create([
                'nama'      => $request->get('nama'),
                'jumlah'    => $request->get('jumlah'),
                'id_user'   => Auth::guard('web')->id(),
            ]);

            return redirect()->route('home')->with('status', 'Produk telah berhasil ditambahkan!');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $view = 'Lihat';
        $data = Produk::find($id);
        return view('form.produk', compact('view', 'data'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = 'Edit';
        $data = Produk::find($id);
        return view('form.produk', compact('edit', 'data'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'      => ['required', 'string', 'max:255'],
            'jumlah'    => ['required'],
        ]);

        try {
            $data = Produk::find($id);
            $data->nama = $request->get('nama');
            $data->jumlah = $request->get('jumlah');
            $data->updated_at = date("Y-m-d H:i:s");
            $data->save();

            return redirect()->route('home')->with('status', 'Data produk telah berhasil diubah!');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Produk::find($id)->delete();
            return back()->with('status', 'Data produk telah berhasil dihapus!');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }
}
