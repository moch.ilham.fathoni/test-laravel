<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Produk;
use App\Models\ProdukToko;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth:web', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Order::where('id_supplier', '=', Auth::guard('web')->id())->get();
        return Datatables::of($data)
                         ->addIndexColumn()
                         ->editColumn('tanggal', function ($model) {
                            $date = '';
                            if ($model->tanggal) {
                                $date = Carbon::createFromFormat('Y-m-d', $model->tanggal)->format('d-m-Y');
                            }
                            return $date;
                         })
                         ->addColumn('produk', function ($model) {
                            return $model->produk->nama;
                         })
                         ->addColumn('toko', function ($model) {
                            return $model->toko->name;
                         })
                         ->addColumn('status', function ($model) {
                            return $model->approve ? 'Sudah Diapprove' : 'Belum Diapprove';
                         })
                         ->addColumn('action', function ($model) {
                            return $model->approve ? 'Tidak ada' : view('action.order', compact('model'))->render();
                         })
                         ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $order = Order::find($id);
            $produk = Produk::find($order->id_produk);

            if (!$order->approve) {
                $produk->jumlah = $produk->jumlah - $order->jumlah;
                $produk->updated_at = date("Y-m-d H:i:s");
                $produk->save();

                ProdukToko::create([
                    'id_user'   => $order->id_toko,
                    'id_produk' => $order->id_produk,
                    'jumlah'    => $order->jumlah,
                ]);

                $order->approve = true;
                $order->updated_at = date("Y-m-d H:i:s");
                $order->save();
            }

            return redirect()->route('order.index')->with('status', 'Approval order ' . $order->produk->nama . ' telah berhasil!');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
