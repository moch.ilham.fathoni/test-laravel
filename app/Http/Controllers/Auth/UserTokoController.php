<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UserToko;
use Illuminate\Http\Request;

class UserTokoController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users_toko'],
                'username' => ['required', 'string', 'max:100', 'unique:users_toko'],
                'password' => ['required', 'string', 'min:8', 'confirmed']
            ]);
            $validatedData['password'] = bcrypt($request->password);

            $user = UserToko::create($validatedData);
            $accessToken = $user->createToken('authToken')->accessToken;
            return response(['success' => true, 'user' => $user, 'access_token' => $accessToken, 'message' => 'Register successfully']);
        } catch (\Exception $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        } catch (\Throwable $e) {
            return response(['success' => false, 'message' =>  $e->getMessage()]);
        }
    }

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        if (!auth()->guard('toko')->attempt($loginData)) {
            return response(['success' => false, 'message' => 'Invalid Credentials']);
        }
        $accessToken = auth()->guard('toko')->user()->createToken('authToken')->accessToken;

        return response(['success' => true, 'user' => auth()->guard('toko')->user(), 'access_token' => $accessToken]);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json(
            [
                'message' => 'Logged out'
            ]
        );
    }
}
