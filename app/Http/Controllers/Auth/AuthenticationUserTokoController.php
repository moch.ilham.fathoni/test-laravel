<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Validator;

class AuthenticationUserTokoController extends Controller
{
    public function guard()
    {
        return Auth::guard('toko');
    }

    public function broker()
    {
        return Password::broker('tokos');
    }

    public function forgotPassword(Request $request)
    {
        $input = $request->only('email');
        $validator = Validator::make($input, [
            'email' => "required|email"
        ]);
        if ($validator->fails()) {
            return response(['success' => false, 'message' => $validator->errors()->all()], 422);
        }
        $response =  $this->broker()->sendResetLink($input);
        if($response){
            $message = "Mail send successfully";
        }else{
            $message = "Email could not be sent to this email address";
        }
        $response = ['success' => true, 'message' => $message];
        return response($response, 200);
    }

    public function reset(Request $request)
    {
        $input = $request->only('email', 'token', 'password', 'password_confirmation');
        $validator = Validator::make($input, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);
        if ($validator->fails()) {
            return response(['success' => false, 'message' => $validator->errors()->all()], 422);
        }
        $response = $this->broker()->reset($input, function ($user, $password) {
            $user->forceFill([
                'password' => Hash::make($password)
            ])->save();
            event(new PasswordReset($user));
        });
        if($response == Password::PASSWORD_RESET){
            $message = "Password reset successfully";
        }else{
            $message = "Email could not be sent to this email address";
        }
        $response = ['success' => true, 'message' => $message];
        return response()->json($response);
    }
}
