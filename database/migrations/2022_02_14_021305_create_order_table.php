<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_supplier');
            $table->unsignedBigInteger('id_produk');
            $table->unsignedBigInteger('id_toko');
            $table->integer('jumlah');
            $table->date('tanggal');
            $table->boolean('approve')->default(false);
            $table->timestamps();
            $table->foreign('id_supplier')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('id_produk')
                  ->references('id')
                  ->on('produk')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('id_toko')
                  ->references('id')
                  ->on('users_toko')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
